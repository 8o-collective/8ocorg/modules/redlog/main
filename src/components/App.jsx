import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";

import Redlog from "actions/os";

// import { Directory, TextFile, DirectoryContext } from "actions/os/directories";

import state from "actions/os/state.js";
import commands from "actions/commands.js";

import { AppContainer } from "assets/styles/App.styles.jsx";

const defaultStyle = { color: "red", fontFamily: "IBM3270", fontSize: "12px" };

// TOOD: Make the UI fullscreen

const welcomeMessage = `
BOOTING INTO REDLOG... INITIALIZED.
YOU ARE EXPERIENCING:
VERSION: v0.0.0
CODENAME: PROTOTYPICAL
`; // TODO: make this check the ENV

const App = () => {
  [state.locked.get, state.locked.set] = useState(false); // I have NO EARTHLY IDEA WHY but these need semicolons????
  [state.disabled.get, state.disabled.set] = useState(false);
  [state.pwd.get, state.pwd.set] = useState("/");

  const { path } = useParams();

  useEffect(() => {
    if (path) {
      try {
        window.dc.changeDir(decodeURIComponent(path));
        state.pwd.set(window.dc.workingDir.name);
      } catch (e) {
        console.log(e);
      }
    }
  }, []);

  return (
    <AppContainer>
      <Redlog
        ref={state.terminal}
        commands={commands}
        disabled={state.disabled.get}
        locked={state.locked.get}
        // contentStyle={defaultStyle} // Text color, family
        inputTextStyle={defaultStyle} // Input text color, family
        promptLabel={<b>{`guest@redlog.8oc.org ${state.pwd.get} >`}</b>}
        promptLabelStyle={{ flexShrink: 0, ...defaultStyle }}
        style={{ backgroundColor: "black", height: "100%" }}
        styleEchoBack="fullInherit" // Inherit echo styling from prompt
        contentStyle={{ padding: "0px 5px", ...defaultStyle }}
        welcomeMessage={welcomeMessage}
        errorText={`COMMAND [command] NOT FOUND. USE 'help' COMMAND FOR INSTRUCTIONS.`}
        autoFocus
        noDefaults
        hidePromptWhenDisabled
      />
    </AppContainer>
  );
};

export default App;
