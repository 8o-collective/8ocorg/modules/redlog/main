import React from "react";
import ReactDOM from "react-dom/client";
import { BrowserRouter as Router } from "react-router-dom";

import { routes } from "routes.js";

// render the main component
ReactDOM.createRoot(document.getElementById("root")).render(
  <Router>{routes}</Router>
);
