import styled from "styled-components";

const AppContainer = styled.div`
  background-color: black;
  margin: 0px;
  position: absolute;
  width: 100vw;
  height: 100vh;
  top: 0px;
  left: 0px;
`;

export { AppContainer };
