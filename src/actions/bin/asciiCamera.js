// originally stolen from https://github.com/idevelop/ascii-camera, now rewritten

const bound = (value, interval) =>
  Math.max(interval[0], Math.min(interval[1], value));

const getColorAtOffset = (data, offset) => ({
  red: data[offset],
  green: data[offset + 1],
  blue: data[offset + 2],
  alpha: data[offset + 3],
});

class asciiCamera {
  constructor({
    width = 640,
    height = 480,
    fps = 30,
    contrast = 128,
    mirror = false,
    targetCanvas = null,
    onSuccess = () => {},
    onError = () => {},
    onNotSupported = () => {},
    onFrame = () => {},
  } = {}) {
    this.width = width;
    this.height = height;
    this.fps = fps;
    this.contrast = contrast;
    this.mirror = mirror;
    this.targetCanvas = targetCanvas;

    // Callbacks
    this.onSuccess = onSuccess;
    this.onError = onError;
    this.onNotSupported = onNotSupported;
    this.onFrame = onFrame;

    this.initvideoStream();
  }

  initvideoStream() {
    this.video = document.createElement("video");
    this.video.setAttribute("width", this.width);
    this.video.setAttribute("height", this.height);
    this.video.setAttribute("playsinline", "true");
    this.video.setAttribute("webkit-playsinline", "true");

    navigator.getUserMedia =
      navigator.getUserMedia ||
      navigator.webkitGetUserMedia ||
      navigator.mozGetUserMedia ||
      navigator.msGetUserMedia;
    window.URL =
      window.URL || window.webkitURL || window.mozURL || window.msURL;

    if (!navigator.getUserMedia) {
      this.onNotSupported();
    } else {
      navigator.getUserMedia(
        { video: true, audio: false },
        (stream) => {
          if (this.video.mozSrcObject !== undefined) {
            // hack for Firefox < 19
            this.video.mozSrcObject = stream;
          } else {
            this.video.srcObject = stream;
          }

          this.initCanvas();
        },
        this.onError
      );
    }
  }

  initCanvas() {
    this.canvas = this.targetCanvas || document.createElement("canvas");
    this.canvas.setAttribute("width", this.width);
    this.canvas.setAttribute("height", this.height);

    let context = this.canvas.getContext("2d");

    // mirror this.video
    if (this.mirror) {
      context.translate(this.canvas.width, 0);
      context.scale(-1, 1);
    }

    this.onSuccess();
  }

  start() {
    this.video.play();

    this.renderTimer = setInterval(() => {
      try {
        this.canvas
          .getContext("2d")
          .drawImage(this.video, 0, 0, this.video.width, this.video.height);
        this.asciiFromCanvas();
      } catch (e) {
        // TODO
      }
    }, Math.round(1000 / this.fps));
  }

  stop() {
    if (this.renderTimer) clearInterval(this.renderTimer);
    this.video.pause();

    let videoSrc =
      this.video.mozSrcObject !== undefined
        ? this.video.mozSrcObject
        : this.video.srcObject;

    videoSrc.getTracks().forEach(function (track) {
      track.stop();
    });

    videoSrc = null;
  }

  asciiFromCanvas() {
    // Original code by Jacob Seidelin (http://www.nihilogic.dk/labs/jsascii/)
    // Heavily modified by Andrei Gheorghe (http://github.com/idevelop)

    // var characters = (" .,:;i1tfLCG08@").split("");
    const characters = "@80GCLftli;:,.".split("").concat(["\xa0"]); // get characters and add space to the end

    let asciiCharacters = "";

    // calculate contrast factor
    // http://www.dfstudios.co.uk/articles/image-processing-algorithms-part-5/
    const contrastFactor =
      (259 * (this.contrast + 255)) / (255 * (259 - this.contrast));

    const imageData = this.canvas
      .getContext("2d")
      .getImageData(0, 0, this.canvas.width, this.canvas.height);

    for (let y = 0; y < this.canvas.height; y += 2) {
      // every other row because letters are not square
      for (let x = 0; x < this.canvas.width; x++) {
        // get each pixel's brightness and output corresponding character

        let offset = (y * this.canvas.width + x) * 4;

        let color = getColorAtOffset(imageData.data, offset);

        // increase the contrast of the image so that the ASCII representation looks better
        // http://www.dfstudios.co.uk/articles/image-processing-algorithms-part-5/
        let contrastedColor = {
          red: bound(
            Math.floor((color.red - 128) * contrastFactor) + 128,
            [0, 255]
          ),
          green: bound(
            Math.floor((color.green - 128) * contrastFactor) + 128,
            [0, 255]
          ),
          blue: bound(
            Math.floor((color.blue - 128) * contrastFactor) + 128,
            [0, 255]
          ),
          alpha: color.alpha,
        };

        // calculate pixel brightness
        // http://stackoverflow.com/questions/596216/formula-to-determine-brightness-of-rgb-color
        let brightness =
          (0.299 * contrastedColor.red +
            0.587 * contrastedColor.green +
            0.114 * contrastedColor.blue) /
          255;

        let character =
          characters[
            characters.length -
              1 -
              Math.round(brightness * (characters.length - 1))
          ];

        asciiCharacters += character;
      }

      asciiCharacters += "\n";
    }

    this.onFrame(asciiCharacters);
  }
}

export default asciiCamera;
