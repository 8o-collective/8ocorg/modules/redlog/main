import state from "./os/state";
import { terminalLog } from "./os/logging";

import fgnu from "./os/fgnu";

import asciiCamera from "./bin/asciiCamera";

const help = {
  description: "SEE ALL AVENUES",
  usage: "help",

  fn: () => {
    let message = "";

    for (const c in commands) {
      const cmdObj = commands[c];
      // const usage = cmdObj.usage ? ` - ${cmdObj.usage}` : ''

      // message += `${c} - ${cmdObj.description}${cmdObj.usage}\n`
      message += `${c} - ${cmdObj.description}\n`;
    }

    return message;
  },
};

const profiler = {
  description: "AN IMPERFECT MIRROR",
  usage: "profiler",
  fn: () => {
    // const terminal = state.terminal.current;

    state.disabled.set(true);

    const SECONDS_OF_STARTUP = 2;
    const SECONDS_OF_VIDEO = 8;

    const camera = new asciiCamera({
      width: 100,
      height: 50,
      fps: 30,
      mirror: true,

      onFrame: (asciiString) => terminalLog(asciiString),

      onSuccess: () => {
        terminalLog("Initializing camera...");

        // Running and updating
        setTimeout(() => {
          state.locked.set(true);
          camera.start();
        }, SECONDS_OF_STARTUP * 1000);

        // Killing and cleaning
        setTimeout(() => {
          state.disabled.set(false);
          state.locked.set(false);
          camera.stop();
        }, SECONDS_OF_STARTUP * 1000 + SECONDS_OF_VIDEO * 1000);
      },

      onError: () => {
        state.disabled.set(false);
        state.locked.set(false);
        terminalLog(
          "Unfortuntely, you've blocked us from looking at you. What a shame."
        );
      },

      onNotSupported: () => {
        state.disabled.set(false);
        state.locked.set(false);
        terminalLog("Current machine doesn't support this functionality.");
      },
    });
  },
};

const commands = {
  help: help,
  profiler: profiler,
  ...fgnu,
};

export default commands;
