import state from "./state.js";

import { DirectoryContext } from "./directories.js";

let dc = new DirectoryContext();
window.dc = dc;

const clear = {
  description: "FORGET THE PAST",
  usage: "clear",
  fn: () => {
    state.terminal.current.clearStdout();
  },
};

const ls = {
  description: "OPEN VISTAS OF REALITY AND OF YOUR LOCATION THEREIN",
  usage: "ls",
  fn: (...parameters) => {
    window.dc.workingDir.sortChildren();
    let entries = window.dc.workingDir.children;

    if (parameters.length > 0) {
      if (parameters.length > 1) {
        throw new Error("More than one argument given.");
      }

      let path = parameters[0];

      entries = window.dc.getPathFromPathString(path).at(-1).children;
    }

    if (entries === null) {
      return "Not a directory.";
    }

    return entries.map(
      (entry) => `${entry.name.trim()}${entry.children !== null ? "/" : ""}\n`
    );
  },
};

const cd = {
  description: "CHANGE THE WORLD",
  usage: "cd [path]",
  fn: (...parameters) => {
    try {
      if (parameters.length > 1) {
        throw new Error("More than one argument given.");
      }

      let path = parameters[0];

      window.dc.changeDir(path);
      state.pwd.set(window.dc.workingDir.name);
    } catch (e) {
      return `Could not change directory: ${e}`;
    }
  },
};

const pwd = {
  description: "SHOW ME YOU WHERE YOU ARE",
  usage: "pwd",
  // fn: (...parameters) => {
  fn: () => {
    try {
      return window.dc.workingDir.readablePath;
    } catch (e) {
      return `Could not change directory: ${e}`;
    }
  },
};

const mkdir = {
  description: "ESTABLISH A FOOTHOLD DEEPER",
  usage: "mkdir [path]",
  fn: (...parameters) => {
    for (const path of parameters) {
      try {
        window.dc.makeDir(path);
      } catch (e) {
        return `Could not make directory: ${e}`;
      }
    }
  },
};

const touch = {
  description: "RUN YOUR HAND ALONG A SPACE",
  usage: "touch [path]",
  fn: (...parameters) => {
    for (const path of parameters) {
      try {
        window.dc.touch(path);
      } catch (e) {
        return `Could not touch file: ${e}`;
      }
    }
  },
};

const cat = {
  description: "CATS. CATS EVERYWHERE.",
  usage: "cat [path]",
  fn: (...parameters) => {
    for (const path of parameters) {
      try {
        return window.dc.getPathFromPathString(path).at(-1).content;
      } catch (e) {
        return `Could not read file: ${e}`;
      }
    }
  },
};

const fgnu = {
  clear: clear,
  ls: ls,
  cd: cd,
  mkdir: mkdir,
  touch: touch,
  cat: cat,
  pwd: pwd,
};

export default fgnu;
