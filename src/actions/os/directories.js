import base64 from "base-64";

const globalUser = "SYSTEM";
const defaultUser = "Guest";

const generateRoot = (dc, serializedDirectory) => {
  const walk = (entry) => {
    if (entry.children) {
      if (entry.path) {
        dc.makeDir(entry.path);
      }
      entry.children.forEach((child) => {
        walk(child);
      });
    } else {
      let file = dc.touch(entry.path);
      file.fileContent = entry.content;
    }
  };

  walk(serializedDirectory);
};

const removeLastEntryFromStringPath = (string) => {
  let potentialPath =
    (string[0] === "/" ? "/" : "") +
    string.slice(0, -1).split("/").slice(0, -1).join("/");

  potentialPath = potentialPath.replaceAll("//", "/");
  return potentialPath ? potentialPath : ".";
};

const getLastEntryFromStringPath = (string) => {
  if (string.includes("//")) {
    throw new Error("Could not parse path.");
  }

  let possiblePath = string.split("/");

  if (possiblePath.at(-1) === "" && possiblePath.at(-2) !== "") {
    return possiblePath.at(-2); // this is ok because of the parse check earlier
  } else if (possiblePath.at(-1) === "") {
    return "/";
  }

  return possiblePath.at(-1);
};

// Paths are always an array of objects
class Entry {
  constructor(name, parent, author, timestamp) {
    if (new.target === Entry) {
      throw new TypeError("Cannot construct Abstract instances directly.");
    }

    this.type = undefined;

    name = name.trim();

    this.name = name;
    this.parent = parent;
    this.children = null;
    this.author = author;
    this.timestamp = timestamp;
  }

  get size() {
    return undefined;
  }

  get content() {
    throw new Error("File has no content.");
  }

  get path() {
    const depth_cap = 25; // safety precautions :]
    let recurses = 0;

    let path = [this];
    let current = this;

    while (current.name !== "/" && recurses < depth_cap) {
      current = current.parent;
      path.push(current);
      recurses++;
    }

    return path.reverse();
  }

  // returns a path in printable format, for i/o
  get readablePath() {
    let pathStr = "";
    let pathArray = this.path;

    // if (!pathArray[0].name) {
    //   pathStr += "/";
    // } else {
    //   pathStr += "?/";
    // }

    pathStr += "/";

    for (let i = 1; i < pathArray.length; i++) {
      pathStr += pathArray[i].name;
      if (pathArray[i].children !== null && i != pathArray.length - 1) {
        pathStr += "/";
      }
    }

    pathStr = pathStr.replaceAll("//", "/");
    return pathStr;
  }
}

class ContentFile extends Entry {
  constructor(name, parent, author, timestamp = Date.now(), content = "") {
    super(name, parent, author, timestamp);
    this.fileContent = content;
  }

  get content() {
    return this.fileContent;
  }

  get size() {
    return this.content.length;
  }
}

class Directory extends Entry {
  constructor(name, parent, author, children = [], timestamp = Date.now()) {
    super(name, parent, author, timestamp);
    this.children = children;
  }

  get content() {
    throw new Error("Cannot read content from a Directory.");
  }

  addChild(child) {
    if (this.findChild(child.name) !== undefined) {
      throw new Error("File or directory already exists at this location.");
    }

    this.children.push(child);
  }

  findChild(name) {
    return this.children.find((child) => child.name === name);
  }

  findChildFromFragment(fragment) {
    return this.children.find((child) => child.name.startsWith(fragment));
  }

  removeChild(name) {
    this.children = this.children.filter((child) => child.name !== name);
  }

  sortChildren() {
    this.children.sort((a, b) => a.name.localeCompare(b.name));
  }

  get size() {
    return this.children.reduce((sum, child) => (sum += child.size()));
  }
}

class DirectoryContext {
  constructor(
    root = new Directory("/", null, globalUser),
    username = defaultUser
  ) {
    this.root = root;
    this.root.parent = root;
    this.workingDir = root;

    this.username = username;

    generateRoot(this, JSON.parse(base64.decode(window.serializedRoot)));
  }

  // path is a string here
  // '/'
  getPathFromPathString(pathString) {
    if (pathString.length === 0) {
      throw new Error("No path specified.");
    }

    let pathStringArray = pathString.split("/");
    if (pathString.at(-1) === "/") {
      pathStringArray = pathStringArray.slice(0, -1);
    }

    let path;

    if (pathString[0] === "/") {
      path = [this.root];
      pathStringArray.shift(); // return if the only thing is root

      if (pathStringArray.at(-1) === "") {
        return path;
      }
    } else if (pathStringArray[0] === "..") {
      path = [this.workingDir.parent];

      if (pathStringArray.length === 1) {
        return path;
      }
    } else {
      path = [this.workingDir];

      if (pathStringArray === ["."]) {
        return path;
      }
    }

    for (let [index, entryString] of pathStringArray.entries()) {
      if (entryString === ".") {
        path.push(path[index]);
        continue;
      } else if (entryString === "..") {
        path.push(path[index].parent);
        continue;
      }

      let possibleEntry = path[index].findChild(entryString);

      if (possibleEntry) {
        path.push(possibleEntry);
      } else {
        throw new Error("Path does not exist.");
      }
    }

    if (path[0].name !== "/") {
      path = [...this.workingDir.parent.path, ...path];
    }

    return path;
  }

  changeDir(path) {
    let dir = this.getPathFromPathString(path).at(-1);

    if (dir.children === null) {
      throw new Error("Cannot change directory into a file.");
    }

    this.workingDir = dir;
  }

  remove(path) {
    let filePath = this.getPathFromPathString(path);
    let file = filePath.at(-1);

    // if this.workingDir.path.contains(file) {
    //   throw new Error('Cannot delete a parent of the working directory.')
    // }

    filePath.at(filePath.length - 2).removeChild(file);
  }

  makeDir(path, timestamp = Date.now()) {
    let parentDirectory = this.getPathFromPathString(
      removeLastEntryFromStringPath(path)
    ).at(-1);

    let name = getLastEntryFromStringPath(path);

    if (name === ".." || name === "." || name === "/") {
      throw new Error("Invalid path.");
    }

    if (parentDirectory.children === null) {
      throw new Error("Cannot create file inside of a non-directory.");
    }

    let dir = new Directory(
      name,
      parentDirectory,
      this.username,
      [],
      timestamp
    );
    parentDirectory.addChild(dir);
    return dir;
  }

  touch(path, timestamp = Date.now()) {
    try {
      let file = this.getPathFromPathString(path).at(-1);

      file.timestamp = Date.now();
      return file;
    } catch (e) {
      let parentDirectory = this.getPathFromPathString(
        removeLastEntryFromStringPath(path)
      ).at(-1);

      if (parentDirectory.children === null) {
        throw new Error("Cannot create file inside of a non-directory.");
      }

      let file = new ContentFile(
        getLastEntryFromStringPath(path),
        parentDirectory,
        this.username,
        timestamp
      );
      parentDirectory.addChild(file);
      return file;
    }
  }

  /* 
  loadRootFromDirectory(root){
    
  }
  */
}

export { DirectoryContext };
