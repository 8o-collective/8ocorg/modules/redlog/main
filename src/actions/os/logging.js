import state from "./state.js";

const terminalLog = (string) => {
  state.terminal.current.pushToStdout(string);
  state.terminal.current.scrollToBottom();
};

export { terminalLog };
