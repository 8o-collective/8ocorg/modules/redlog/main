import Terminal from "react-console-emulator";

export default class RedlogTerminal extends Terminal {
  autoComplete = () => {
    const rawInput = this.terminalInput.current.value;

    if (rawInput) {
      const input = rawInput.split(" ");
      const inputPath = input[input.length - 1];

      const parentInputPath = window.dc.getPathFromPathString(
        inputPath.split("/").slice(0, -1).join("/") ||
          window.dc.workingDir.readablePath
      );

      const inputFragment = inputPath.split("/").at(-1);

      const child = parentInputPath.at(-1).findChildFromFragment(inputFragment);
      const childIsDirectory = child.children !== null;

      if (child) {
        let replacement = rawInput;

        if (inputFragment !== "") {
          const fragmentIndex = rawInput.lastIndexOf(inputFragment);
          replacement =
            rawInput.slice(0, fragmentIndex) +
            child.name +
            rawInput.slice(fragmentIndex + inputFragment.length);
        } else {
          replacement += child.name;
        }

        replacement += childIsDirectory ? "/" : "";

        this.terminalInput.current.value = replacement;
      }
    }
  };

  handleInput = (event) => {
    if (["Tab"].includes(event.key)) {
      event.preventDefault();
    }

    switch (event.key) {
      case "Enter":
        this.processCommand();
        break;
      case "ArrowUp":
        this.scrollHistory("up");
        break;
      case "ArrowDown":
        this.scrollHistory("down");
        break;
      case "Tab":
        this.autoComplete();
        break;
    }
  };
}
