// const webpack = require("webpack");
const path = require("path");
const fs = require("fs");
const InjectPlugin = require("webpack-inject-plugin").default;

const createBuildTemplate = require("template");

const appDirectory = fs.realpathSync(process.cwd());
const resolveAppPath = (relativePath) =>
  path.resolve(appDirectory, relativePath);

const base64 = require("base-64");

const serializeDirectory = require("./os/serializeDirectory.js");
const serializedRoot = serializeDirectory(resolveAppPath("src/assets/os/root"));

module.exports = createBuildTemplate(resolveAppPath, {
  plugins: [
    new InjectPlugin(
      async () =>
        `window.serializedRoot="${base64.encode(
          JSON.stringify(await serializedRoot)
        )}";`
    ),
  ],
});
