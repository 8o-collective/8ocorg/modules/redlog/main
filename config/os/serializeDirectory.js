const fs = require("fs");
const path = require("path");

const serializeDirectory = async (dir) => {
  const walk = entry => {
    return new Promise((resolve, reject) => {
      return resolve(new Promise((resolve, reject) => {
        fs.lstat(entry, (err, stats) => {
          if (err) {
            return reject(err);
          }
          let contextPath = entry.replace(dir, "");
          if (!stats.isDirectory()) {
            return resolve(new Promise((resolve, reject) => {
              fs.readFile(entry, 'utf8', (err, data) => {
                if (err) {
                  return reject(err);
                }
                resolve({
                  path: contextPath,
                  content: data,
                  time: stats.mtime,
                  size: stats.size
                })
              })
            }))
          }
          resolve(new Promise((resolve, reject) => {
            fs.readdir(entry, (err, files) => {
              if (err) {
                return reject(err);
              }
              Promise.all(files.map(child => walk(path.join(entry, child)))).then(children => {
                resolve({
                  path: contextPath,
                  time: stats.mtime,
                  children
                });
              }).catch(err => {
                reject(err);
              });
            });
          }));
        });
      }));
    });
  }

  return walk(dir);
}

module.exports = serializeDirectory;